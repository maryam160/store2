
import React from 'react'
import CardHeader from '../card/CardHeader'
import ProductCard from '../card/ProductCard'



const product=[
  { image: '/assets/images/orange.jpg', title: 'پرتقال خونی', unit: 'kg', price: 43000, discountPrice: 23000, _id: 1 },
  { image: '/assets/images/orange.jpg', title: 'پرتقال خونی', unit: 'kg', price: 43000, discountPrice: 0, _id: 2 }
]


function Product() {

  return (
    <>
   <CardHeader text='محصولات' />
   {
        product.map((item, i) => (
          <ProductCard data={item} key={i} />
        ))
      }

   </>
    
  )
}

export default Product
