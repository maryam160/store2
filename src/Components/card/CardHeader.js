import React from 'react'
import styled from 'styled-components'
import { darkGray } from '../../common/styles/theme'


const HeaderCard=styled.div`
position:relative;
margin: 29px 0;
  
  hr{
    color:${darkGray};
    
  }

  div{
    position:absolute;
    left:0;
    right:0;
    display:flex;
    justify-content:center;
    top:-17px;

     p{
       background-color:white;
       padding:0 20px;
       font-size:20px;
       color:${darkGray};
     }
  }

`


function CardHeader({ text }) {
  return (
    <HeaderCard >
      <hr />
      <div>
        <p> {text}</p>
      </div>
    </HeaderCard>
  )
}

export default CardHeader
