import React, { useState } from 'react'
import styled from 'styled-components'
import { Row } from 'react-bootstrap'
import { orangeColor } from '../../common/styles/theme'
import BasketCounter from '../Counter/BasketCounter'


const RowProduct = styled(Row)`
position:relative;
margin-right:7px;
-webkit-box-shadow: 0px 0px 25px -2px rgba(214,214,214,1);
-moz-box-shadow: 0px 0px 25px -2px rgba(214,214,214,1);
box-shadow: 0px 0px 25px -2px rgba(214,214,214,1);
margin-top: 30px;
  
  img{
    width: 35%;
    object-fit: cover;
  }
  .procuct-info{
    width: 65%;
    p {
      font-size: 12px;
    }
    .row:nth-child(odd){
      background-color: white;
    }
    .row:nth-child(even){
      background-color: $card-gray;
    }
    .color-price{
      color: ${orangeColor};
    }
  }

`
const ButtonContainer=styled.div`
position:absolute;
right:-10px;
top:0;
bottom:0;
display:flex;
align-items:center;
`
const DeleteButton=styled.button`
background-color:${orangeColor};
color:white;
border: none;
padding-bottom: 6px;
height: 30px;
width: 30px;
line-height: 0px;
text-align: center;
font-size: 22px;
border-radius: 50%;

`


function ProductCard({data ,deleteHandler}) {
  const [count, setCount] = useState(1)
  return (
    <RowProduct noGutters >
      <ButtonContainer>
        <DeleteButton onClick={deleteHandler}>
          x
        </DeleteButton>
      </ButtonContainer>
      <BasketCounter count={count} handler={setCount}  />
     

      <img src='/assets/images/orange.jpg'/>
      <div className='product-info'>
        <Row noGutters>
          <p>
            {data.title}
          </p>
        </Row>
        <Row noGutters>


        </Row>
         <Row noGutters>


         </Row>


      </div>
      
      
    </RowProduct>
  )
}

export default ProductCard
