import React from 'react'
import styled from 'styled-components'
import { darkGray, orangeColor } from '../../common/styles/theme'

const Container=styled.div`
position:absolute;
top:8px;
bottom:8px;
display:flex;
right:31%;
align-items:center;
justify-content:space-between;
background-color:white;
div{
  width: 20px;
    height: 20px;
    transform: rotate(90deg);
    border-radius: 50%;
    position: relative;

 button{
   color:white;
   border:none;
   background-color:${orangeColor};
   border-radius:50%;

 }
}

`
const Basket=styled.div`
width:auto;
height:auto;
span{
  position:absolute;
  right:0;
  left:0;
  text-align:center;
  top:5px;
}


`

function BasketCounter({count,Handler}) {
  return (
    <Container >
      <div>
        <button onClick={() =>Handler(s =>s+1)}>&gt;</button>
      </div>
      <Basket>
        <img src="/assets/images/basket.png" alt='' />
        <span></span>
      </Basket>


    </Container>
  )
}

export default BasketCounter
