import React from 'react'
import { Row } from 'react-bootstrap'
import styled from 'styled-components'
import { orangeColor } from '../../common/styles/theme'

const HeaderContainer = styled(Row)`
  color:white;
  background-color: ${orangeColor};

  p {
    color: red;
  }
`

function Header({ title }) {
  return (
    <HeaderContainer noGutters className=' test py-3 px-3'>
      <h5>{title}</h5>

    </HeaderContainer>
  )
}

export default Header
