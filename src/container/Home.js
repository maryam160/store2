import React from 'react'
import Header from '../Components/Header/Header'
import Product from '../Components/product/Product'
import styled from 'styled-components'

const HomeDirection=styled.div`
direction:rtl;

`
const BodyWrapper=styled.div`
padding:0 15px;

`

function Home() {
  return (
    <HomeDirection>
        <Header title='سبد خرید' />
        <BodyWrapper>
            <Product />
      
        </BodyWrapper>
    </HomeDirection>
  )
}

export default Home
